"""
LMS Static_Texts app config.
"""

from django.apps import AppConfig


class ShortedConfig(AppConfig):
    """
    Shorted app config.
    """

    name = "shorted"
    verbose_name = "short/ED"
