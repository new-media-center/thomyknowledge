import logging

from django import template
from django.conf import settings
from django.urls import translate_url as django_translate_url
from shorted.models import StaticText

register = template.Library()


@register.simple_tag(takes_context=True)
def translate_url(context, lang_code):
    """Return translated url."""
    if context.get("request") is None:
        return ""
    path = context.get("request").get_full_path()
    return django_translate_url(path, lang_code)


@register.simple_tag(takes_context=False)
def get_static_text(identifier):
    """Return static text for a given identifier."""
    text = ""
    try:
        text = StaticText.objects.get(text_position=identifier).content
    except StaticText.DoesNotExist:
        if settings.DEBUG:
            text = f"[[StaticText with identifier {identifier} does not exist.]]"
        logger = logging.getLogger(__name__)
        logger.warning(f"StaticText with identifier {identifier} does not exist.")
    return text
