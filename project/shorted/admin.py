"""Admin configuration for shorted app."""

from django.contrib import admin

from .models import Idea, StaticText, Video


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    """Admin class for Video model."""

    list_display = (
        "title",
        "video_url",
        "pinned",
        "archive",
        "oembed_data",
        "preview_image",
        "preview_video_path",
    )
    list_filter = ("pinned", "archive")
    search_fields = ("title", "video_url")


@admin.register(Idea)
class IdeaAdmin(admin.ModelAdmin):
    """Admin class for Idee model."""

    list_display = (
        "email_address",
        "first_name",
        "last_name",
        "idea",
        "answer",
        "consent_to_present_personally",
        "field_of_study",
    )
    list_filter = ("consent_to_present_personally", "field_of_study")
    search_fields = (
        "email_address",
        "first_name",
        "last_name",
        "idea",
        "answer",
        "consent_to_present_personally",
        "field_of_study",
    )


@admin.register(StaticText)
class StaticTextAdmin(admin.ModelAdmin):
    """Admin class for StaticText model."""

    list_display = ("text_position", "content")
    list_filter = ("text_position",)
    search_fields = ("text_position", "content")
