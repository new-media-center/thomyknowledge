from django.urls import path
from django.views.generic import DetailView, TemplateView
from shorted import views
from shorted.models import Video

app_name = "shorted"

urlpatterns = [
    path(
        "",
        views.HomeView.as_view(template_name="pages/home.html"),
        name="home",
    ),
    path(
        "impressum",
        TemplateView.as_view(
            template_name="pages/impressum.html",
        ),
        name="impressum",
    ),
    path(
        "about",
        TemplateView.as_view(
            template_name="pages/about.html",
        ),
        name="about",
    ),
    path(
        "video-detail/<int:pk>",
        DetailView.as_view(
            model=Video,
            template_name="pages/video-detail.html",
        ),
        name="video_detail",
    ),
    path(
        "idea-form",
        views.IdeaFormCreate.as_view(
            template_name="pages/idea-form.html",
        ),
        name="idea-form",
    ),
]
