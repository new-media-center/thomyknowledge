import logging

import requests
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django_q.tasks import async_chain, async_task
from model_utils.models import TimeStampedModel


class Video(TimeStampedModel):
    """
    Model representing a video.
    """

    preview_image = models.ImageField(max_length=255, blank=True)
    preview_video_path = models.CharField(max_length=255, blank=True)
    title = models.CharField(max_length=255, blank=True)
    video_url = models.URLField(
        blank=True,
        default="",
        help_text=_("Link zum Video."),
    )
    pinned = models.BooleanField(
        default=False,
        help_text=_(
            'Anwählen, falls dieses Video als "Gewinner" angepinnt werden soll.'
        ),
    )
    archive = models.BooleanField(
        default=False,
        help_text=_("Anwählen, falls dieses Video im Archiv erscheinen soll."),
    )
    oembed_data = models.JSONField(
        blank=True,
        null=True,
        help_text=_("JSON-Objekt mit den Daten des Videos."),
    )

    def save(self, *args, create_preview=True, **kwargs):
        """
        Override the save method to fetch the oEmbed data for Vimeo videos.
        """
        logger = logging.getLogger(__name__)

        if create_preview and self.video_url.startswith("https://vimeo.com/"):
            logger.log(
                logging.INFO, f"Fetching oEmbed data for Vimeo video {self.video_url}"
            )
            # Construct the oEmbed API URL for the Vimeo video
            api_url = f"https://vimeo.com/api/oembed.json?url={self.video_url}&responsive=true"

            # Make a request to the Vimeo oEmbed API to fetch the oEmbed data
            headers = {"Referer": "unibas.ch"}
            response = requests.get(api_url, headers=headers, timeout=10)

            # If the request was successful, save the oEmbed data to the video object
            if response.status_code == 200:
                oembed_data = response.json()
                logger.log(logging.INFO, oembed_data)
                self.oembed_data = oembed_data
                # Create a video preview gif
                async_chain(
                    [
                        (
                            "shorted.services.download_video",
                            (self.oembed_data["video_id"],),
                        ),
                        ("shorted.services.create_video_preview", (self,)),
                        #    ("shorted.services.delete_source_video", (self.oembed_data["video_id"],)),
                    ]
                )

        super().save(*args, **kwargs)

    def get_absolute_url(self):
        """Get absolute url."""
        return reverse("shorted:video_detail", kwargs={"pk": self.pk})

    def __str__(self):
        """To string method."""
        return self.title


def validate_email_domain(value):
    """
    Validate that the email address is from the unibas.ch domain.
    """
    domain = value.split("@")[-1]
    if domain != "unibas.ch" and domain != "stud.unibas.ch":
        raise ValidationError(
            _(
                "Bitte geben Sie eine gültige @unibas.ch oder @stud.unibas.ch E-Mail Adresse ein."
            )
        )


class Idea(TimeStampedModel):
    """
    Model to record students ideas for new videos.
    """

    email_address = models.EmailField(
        blank=False,
        verbose_name=_("E-Mail Adresse"),
        help_text=_("Ihre @unibas.ch oder @stud.unibas.ch E-Mail Adresse"),
        validators=[validate_email_domain],
    )
    first_name = models.CharField(
        verbose_name=_("Vorname"),
        max_length=40,
        blank=False,
        help_text=_("Ihr Vorname"),
    )
    last_name = models.CharField(
        verbose_name=_("Nachname"),
        max_length=40,
        blank=False,
        help_text=_("Ihr Nachname"),
    )
    idea = models.TextField(
        verbose_name=_("Frage"),
        blank=False,
        help_text=_("Frage, die das Erklärvideo beantworten sollte"),
    )
    answer = models.TextField(
        verbose_name=_("Antwort"),
        blank=False,
        help_text=_(
            "Kurzer Abschnitt, der die Antwort auf ihre Frage gibt."
            "(Bitte beachten Sie, dass das Team New Media Center das Drehbuch der Ideen erstellen wird,"
            "die ausgewählt werden. Sie sehen aber das Drehbuch, bevor wir es umsetzen)."
        ),
    )
    consent_to_present_personally = models.BooleanField(
        verbose_name=_("Einverständnis"),
        help_text=_(
            "Ich wäre bereit, im Video Abschnitte vor der Kamera zu präsentieren."
        ),
    )
    field_of_study = models.CharField(
        verbose_name=_("Studienfach"),
        blank=False,
        help_text=_("Ihr Studienfach"),
    )
    idea_processed = models.BooleanField(
        default=False,
        help_text=_("Die Idee wurde überprüft."),
    )

    def save(self, *args, **kwargs):
        """
        Override the save method to send an email to the user.
        """
        is_new = self.pk is None
        super().save(*args, **kwargs)

        # Send an email to the user when the idea is created
        if is_new:
            async_task(
                "shorted.services.send_email_confirmation", self.email_address, self.id
            )

    def get_success_url(self):
        """Return the url to redirect to after a successful submission."""
        return reverse("shorted:home")

    def __str__(self):
        """Return the string representation of the object."""
        return f"{self.first_name} {self.last_name}: \n{self.idea}"


class StaticText(TimeStampedModel):
    """
    Model to create static texts.
    the value of text_position can be used with the custom template tag {% get_static_text text_position %}
    """

    text_position = models.CharField(max_length=20, blank=False, db_index=True)
    content = models.TextField(
        max_length=None,
        blank=True,
        help_text=_("Text der an dieser Stelle angezeigt werden soll."),
    )

    def __str__(self):
        """Return the string representation of the object."""
        return self.text_position
