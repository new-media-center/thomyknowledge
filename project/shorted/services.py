import logging
import os

import vimeo
from django.conf import settings
from django.core.mail import send_mail
from django.utils.translation import gettext_lazy as _

# Convert a video to a gif
# ffmpeg -i input.mp4 -vf scale=320:-1 -r 10 -f image2pipe -vcodec ppm - | convert -delay 5 -loop 0 - output.gif

logger = logging.getLogger(__name__)


def touch_file(path):
    """
    Create an empty file at the given path.
    """
    os.system(f"touch {path}")


def video_path_builder(video_id, relative_to_media_root=False):
    """
    Return the video path for a video.
    """
    video_filename = f"video_{video_id}.mp4"
    if relative_to_media_root:
        return os.path.join("shorted", "videos", video_filename)
    else:
        return os.path.join(settings.MEDIA_ROOT, "shorted", "videos", video_filename)


def download_video(video_id):
    """
    Download video from vimeo.
    """
    logger.info(f"Downloading video {video_id}")

    # Create a vimeo client
    client = vimeo.VimeoClient(
        token="5adada758ee0c35be72ce753d15e5298",  # noqa
        key="11ce9c5deafc7fd5fd2c55ec906fec92b6f2cc1d",
        secret="gnxjRkC82nGWqrSkxfiswMZxgy9q7sAxe/4X1S177xcTvA0Lph5h1ujeUZ2flltiENcJtNiuWd3F0KZ3YHjjhkBalytkRoMqV3uQCeneY37x4radKU+FuFhsDyZcibwn",  # noqa
    )

    # Fetch the video data
    response = client.get(f"/videos/{video_id}")

    files = response.json()["download"]

    # Find the download url for the 240p video
    for file in files:
        if file["rendition"] == "240p":
            logger.info("Downloading file:", file)
            download_url = file["link"]
            break
        download_url = file["link"]

    # Download the video
    response = client.get(download_url, stream=True)
    response.raise_for_status()

    # Construct the video path
    video_path = video_path_builder(video_id)

    touch_file(video_path)

    # Save the video to the video path
    with open(video_path, "wb") as video_file:
        for chunk in response.iter_content(chunk_size=1024):
            video_file.write(chunk)


# Create a video preview
def create_video_preview(instance):
    """
    Download video from vimeo and create a video preview gif.
    """
    video_id = instance.oembed_data["video_id"]

    logger.log(logging.INFO, f"Creating video preview for video {video_id}")

    # Construct the video preview gif filename
    video_preview_filename = f"video_preview_{video_id}.webp"

    # Construct the video preview gif path
    video_preview_path = os.path.join(
        settings.MEDIA_ROOT, "shorted", "video_previews", video_preview_filename
    )

    touch_file(video_preview_path)

    # Construct the ffmpeg command
    # ffmpeg -ss 0 -t 30 -i video_836443549.mp4 -vf "setpts=0.5*PTS,fps=5,scale=240:-1,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -vcodec libwebp -lossless 0 -compression_level 6 -q:v 50 -loop 0 output.webp # noqa
    ffmpeg_command = [
        "ffmpeg",
        "-y",  # Overwrite output file
        "-ss",  # Start time
        "0",
        "-t",  # Duration
        "30",
        "-i",
        video_path_builder(video_id),
        "-vf",
        '"setpts=0.5*PTS,fps=5,scale=240:-1,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse"',  # Video filter (2x speed, 5 fps, 240p) # noqa
        "-vcodec",
        "libwebp",  # Webp codec
        "-lossless",
        "0",  # Lossy compression
        "-compression_level",
        "6",  # Compression level
        "-q:v",
        "50",  # Quality
        "-loop",
        "0",  # Loop forever
        video_preview_path,
    ]

    cmd = " ".join(ffmpeg_command)

    logger.log(logging.INFO, f"Running ffmpeg command: {cmd}")
    # Run the ffmpeg command
    os.system(cmd)

    # Delete the video
    instance.preview_video_path = video_preview_path
    instance.save(create_preview=False)
    logger.log(logging.INFO, f"Video preview created for video {video_id}")


def delete_source_video(video_id):
    """
    Delete a video.
    """
    logger.log(logging.INFO, f"Deleting video {video_id}")

    # Construct the video path
    video_path = video_path_builder(video_id)

    # Delete the video
    os.remove(video_path)

    logger.log(logging.INFO, f"Video {video_id} deleted")


# Send email confirmation after submitting the form
def send_email_confirmation(email, idea_id):
    """
    Send an email confirmation to the user.
    """
    logger.log(logging.INFO, f"Sending email confirmation to {email}")

    # Construct the email confirmation message
    email_confirmation_message = _("Herzlichen Dank, dass Sie Ihre Idee eingereicht haben! Ungefähr zwei Wochen nach dem nächsten Einsendeschluss nehmen wir Kontakt mit Ihnen auf, um Sie über den Stand der Dinge zu informieren. Wir drücken die Daumen!")  # noqa

    # Send the email confirmation
    send_mail(
        "Idea submitted",
        email_confirmation_message,
        settings.FROM_EMAIL,
        [email],
        fail_silently=False,
    )

    logger.log(logging.INFO, f"Email confirmation sent to {email}")
