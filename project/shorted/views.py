from django.contrib import messages
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from shorted import forms
from shorted.models import Idea, Video


# from watson import SearchView
class HomeView(TemplateView):
    """Home view."""

    def get_context_data(self, **kwargs):
        """Return context data."""
        context = super().get_context_data(**kwargs)
        context["pinned_videos"] = Video.objects.filter(pinned=True)
        context["archived_videos"] = Video.objects.filter(pinned=False)
        return context


class IdeaFormCreate(CreateView):
    """Course create view."""

    model = Idea
    form_class = forms.IdeaForm
    template_name = "pages/idea-form.html"

    def get_success_message(self, cleaned_data):
        """Return success message."""
        return _("Ihre Idee wurde erfolgreich abgeschickt.")

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        messages.success(self.request, self.get_success_message(form.cleaned_data))
        return super().form_valid(form)

    def get_success_url(self):
        """Return success url."""
        return reverse("shorted:home")
