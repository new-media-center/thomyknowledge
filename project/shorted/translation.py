from modeltranslation.translator import TranslationOptions, translator
from shorted.models import StaticText


class StaticTextTranslationOptions(TranslationOptions):
    """Add translation options to offering."""

    fields = ("content",)


translator.register(StaticText, StaticTextTranslationOptions)
