"""
Forms for the shorted app.
"""
from django import forms
from shorted import models


class IdeaForm(forms.ModelForm):
    """Idea form."""

    def clean(self):
        """Clean form data."""
        cleaned_data = super().clean()
        cleaned_data["email_address"] = cleaned_data["email_address"].lower()
        return cleaned_data

    class Meta:
        """Meta class."""

        model = models.Idea
        fields = "__all__"
        exclude = ("idea_processed",)
