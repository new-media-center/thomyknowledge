"""
Base settings to build other settings files upon.
"""

import os

from django.utils.translation import gettext_lazy as _

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
BASE_DIR = os.path.join(BASE_DIR, "../")
PROJECT_DIR = os.path.join(BASE_DIR)

AUTHENTICATION_BACKENDS = ["django.contrib.auth.backends.ModelBackend"]

SECRET_KEY = os.environ["DJANGO_SECRET"]

DEBUG = False

ALLOWED_HOSTS = [
    # '.unibas.ch',
    "*"
]

# Configure sites framework
# See https://docs.djangoproject.com/en/1.10/ref/settings/#sites
SITE_ID = 1

DJANGO_APPS = (
    # Default Django apps:
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # flatpages
    "django.contrib.sites",
    "django.contrib.flatpages",
)

THIRD_PARTY_APPS = (
    "modeltranslation",
    "django.contrib.admin",  # modeltranslation needs to be loaded before admin
    "django_extensions",
    "crispy_forms",
    "crispy_bootstrap5",
    "django_q",
    "markdownify.apps.MarkdownifyConfig",
)

LOCAL_APPS = (
    # Your stuff: custom apps go here
    "shorted",
)


INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS
# Application definition

MIDDLEWARE = (
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
)

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = "config.urls"

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "OPTIONS": {
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            "loaders": [
                "django.template.loaders.filesystem.Loader",
                "django.template.loaders.app_directories.Loader",
            ],
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

# Internationalization
# https://docs.djangoproject.com/en/4.2/topics/i18n/

LANGUAGE_CODE = "de"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ("de", _("Deutsch")),
    ("en", _("Englisch")),
)

LOCALE_PATHS = [
    "/app/project/locale",
]
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_ROOT = str(os.path.join(PROJECT_DIR, "../staticfiles"))

STATIC_URL = "static/"

# See:
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

MEDIA_URL = "/media/"

# See:
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS

MEDIA_ROOT = os.path.join(BASE_DIR, "media")

ADMINS = (("""admin""", "services-nmc@unibas.ch"),)

# Location of root django.contrib.admin URL, use {% url 'admin:index' %}
ADMIN_URL = "admin/"

# Modeltranslation configuration
# ------------------------------------------------------------------------------
MODELTRANSLATION_DEFAULT_LANGUAGE = "de"
MODELTRANSLATION_FALLBACK_LANGUAGES = (
    "de",
    "en",
)

# Crispy forms
CRISPY_TEMPLATE_PACK = "bootstrap5"

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            # Mimicing memcache behavior.
            # https://github.com/jazzband/django-redis#memcached-exceptions-behavior
            "IGNORE_EXCEPTIONS": True,
        },
    }
}

# Configure Django Q for background tasks
Q_CLUSTER = {
    "name": "DJRedis",
    "workers": 4,
    "timeout": 500,
    "retry": 600,
    "django_redis": "default",
}

# Configure email stuff
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
EMAIL_HOST = "smtp.unibas.ch"
EMAIL_PORT = 25

# Don't use SMTP auth for now
# EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER", "")
# EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD", "")
FROM_EMAIL = "contact-nmc@unibas.ch"

MARKDOWNIFY = {
    "default": {
        "WHITELIST_TAGS": [
            'a',
            'abbr',
            'acronym',
            'b',
            'blockquote',
            'em',
            'i',
            'li',
            'ol',
            'p',
            'strong',
            'ul'
            'br'
            'target'
        ],
         "WHITELIST_ATTRS": [
            'href',
            'src',
            'alt',
            'target'
        ]
    },
}