"""
Production settings for Shorted.
"""

import os

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from .base import *  # noqa

SECRET_KEY = os.environ["DJANGO_SECRET"]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("POSTGRES_DB", ""),
        "USER": os.environ.get("POSTGRES_USER", ""),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD", ""),
        "HOST": "shorted-postgres",
        "PORT": "5432",
    },
}

STATIC_ROOT = os.path.join("/app/staticfiles")
MEDIA_ROOT = os.path.join("/app/media")

sentry_sdk.init(
    dsn="https://716980ed9cf746c68fb7be75ebb656c0@o398207.ingest.sentry.io/4505346881028096",
    integrations=[
        DjangoIntegration(),
    ],
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=0.001,
    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True,
)

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
