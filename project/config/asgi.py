"""
ASGI config for Shorted project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/howto/deployment/asgi/
"""

import logging
import os

import django
from django.core.handlers.asgi import ASGIHandler


def get_asgi_application():
    """
    The public interface to Django's ASGI support. Return an ASGI 3 callable.
    Avoids making django.core.handlers.ASGIHandler a public API, in case the
    internal implementation changes or moves in the future.
    """
    django.setup(set_prefix=False)
    return ASGIHandler()


os.environ["DJANGO_SETTINGS_MODULE"] = os.getenv(
    "DJANGO_SETTINGS", "config.settings.production"
)

logger = logging.getLogger(__name__)
logger.info("Django ASGI application is loading")
logger.info(f"Using DJANGO_SETTINGS_MODULE: {os.environ['DJANGO_SETTINGS_MODULE']}")
application = get_asgi_application()
