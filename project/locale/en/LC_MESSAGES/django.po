# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-13 13:47+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/settings/base.py:115
msgid "Deutsch"
msgstr "German"

#: config/settings/base.py:116
msgid "Englisch"
msgstr "English"

#: shorted/models.py:23
msgid "Link zum Video."
msgstr "Link to the video."

#: shorted/models.py:28
msgid "Anwählen, falls dieses Video als \"Gewinner\" angepinnt werden soll."
msgstr "Select if this video should be pinned as \"winner\"."

#: shorted/models.py:33
msgid "Anwählen, falls dieses Video im Archiv erscheinen soll."
msgstr "Select if this video should appear in the archive."

#: shorted/models.py:38
msgid "JSON-Objekt mit den Daten des Videos."
msgstr "JSON object with the data of the video."

#: shorted/models.py:94
msgid ""
"Bitte geben Sie eine gültige @unibas.ch oder @stud.unibas.ch E-Mail Adresse "
"ein."
msgstr "Please enter a valid @unibas.ch or @stud.unibas.ch email address."

#: shorted/models.py:106
msgid "E-Mail Adresse"
msgstr "E-Mail address"

#: shorted/models.py:107
msgid "Ihre @unibas.ch oder @stud.unibas.ch E-Mail Adresse"
msgstr "Your @unibas.ch or @stud.unibas.ch E-Mail address"

#: shorted/models.py:111
msgid "Vorname"
msgstr "First name"

#: shorted/models.py:114
msgid "Ihr Vorname"
msgstr "Your first name"

#: shorted/models.py:117
msgid "Nachname"
msgstr "Last name"

#: shorted/models.py:120
msgid "Ihr Nachname"
msgstr "Your last name"

#: shorted/models.py:123
msgid "Frage"
msgstr "Question"

#: shorted/models.py:125
msgid "Frage, die das Erklärvideo beantworten sollte"
msgstr "Question that the explanatory video should answer"

#: shorted/models.py:128
msgid "Antwort"
msgstr "Answer"

#: shorted/models.py:131
msgid ""
"Kurzer Abschnitt, der die Antwort auf ihre Frage gibt.(Bitte beachten Sie, "
"dass das Team New Media Center das Drehbuch der Ideen erstellen wird,die "
"ausgewählt werden. Sie sehen aber das Drehbuch, bevor wir es umsetzen)."
msgstr ""
"Short paragraph that gives the answer to the question (Please note that the "
"New Media Centre team will script the ideas that are selected. However, you "
"will see the script before we produce it)."

#: shorted/models.py:137
msgid "Einverständnis"
msgstr "Consent"

#: shorted/models.py:139
msgid "Ich wäre bereit, im Video Abschnitte vor der Kamera zu präsentieren."
msgstr "I would be willing to present sections on camera in the video."

#: shorted/models.py:143
msgid "Studienfach"
msgstr "Subject you are studying"

#: shorted/models.py:145
msgid "Ihr Studienfach"
msgstr "Your field of study"

#: shorted/models.py:149
msgid "Die Idee wurde überprüft."
msgstr "The idea has been checked."

#: shorted/models.py:184
msgid "Text der an dieser Stelle angezeigt werden soll."
msgstr "Text to be displayed at this point."

#: shorted/services.py:154
msgid ""
"Herzlichen Dank, dass Sie Ihre Idee eingereicht haben! Ungefähr zwei Wochen "
"nach dem nächsten Einsendeschluss nehmen wir Kontakt mit Ihnen auf, um Sie "
"über den Stand der Dinge zu informieren. Wir drücken die Daumen!"
msgstr ""
"Thank you very much for submitting your idea! Approximately two weeks after "
"the next deadline, we will contact you to let you know how things are going. "
"We are keeping our fingers crossed!"

#: shorted/templates/404.html:8 shorted/templates/404.html:12
msgid "404 - Seite nicht gefunden"
msgstr "404 - Page not found"

#: shorted/templates/404.html:99
msgid "Diese Seite konnte nicht gefunden werden"
msgstr "This page could not be found"

#: shorted/templates/404.html:100
msgid "Zurück zur Startseite"
msgstr "Back to the homepage"

#: shorted/templates/500.html:6
msgid "Looks like something went wrong!"
msgstr ""

#: shorted/templates/500.html:8
msgid ""
"We track these errors automatically, but if the problem persists feel free "
"to contact us. In the meantime, try refreshing."
msgstr ""

#: shorted/templates/base.html:13
msgid "Short/ED - Universität Basel"
msgstr "short/ED - University of Basel"

#: shorted/templates/components/footer.html:8
msgid "Impressum"
msgstr "Imprint"

#: shorted/templates/components/footer.html:13
#: shorted/templates/components/footer.html:14
msgid "Info"
msgstr "Info"

#: shorted/templates/components/header.html:10
msgid "Übersicht"
msgstr "Overview"

#: shorted/templates/components/header.html:18
msgid "Sprache"
msgstr "Language"

#: shorted/templates/pages/about.html:7
msgid "Titelus"
msgstr ""

#: shorted/templates/pages/about.html:10
msgid ""
"\n"
"          Bla bla bli blumm bla bla Bla bla bli blumm bla bla Bla bla bli "
"blumm bla bla\n"
"          Bla bla bli blumm bla bla Bla bla bli blumm bla bla Bla bla bli "
"blumm bla bla\n"
"          Bla bla bli blumm bla bla Bla bla bli blumm bla bla Bla bla bli "
"blumm bla bla\n"
"          Bla bla bli blumm bla bla Bla bla bli blumm bla bla Bla bla bli "
"blumm bla Bla\n"
"          Bla bla bli blumm bla bla Bla bla bli blumm bla bla Bla bla bli "
"blumm bla bla\n"
"          Bla bla bli blumm bla bla Bla bla bli blumm bla bla Bla bla bli "
"blumm bla bla\n"
"          Bla bla bli blumm bla bla Bla bla bli blumm bla bla Bla bla bli "
"blumm bla bla\n"
"          Bla bla bli blumm bla bla Bla bla bli blumm bla bla Bla bla bli "
"blumm bla bla\n"
"          "
msgstr ""

#: shorted/templates/pages/contact.html:5
#: shorted/templates/pages/contact.html:11
msgid "Kontakt"
msgstr "Contact"

#: shorted/templates/pages/contact.html:22
#: shorted/templates/pages/contact.html:59
msgid "Schweiz"
msgstr "Switzerland"

#: shorted/templates/pages/contact.html:31
#: shorted/templates/pages/contact.html:68
msgid "Bürozeiten"
msgstr "Office hours"

#: shorted/templates/pages/contact.html:40
#: shorted/templates/pages/contact.html:70
msgid "Haltestelle Universitätsspital"
msgstr "Universitätsspital stop"

#: shorted/templates/pages/home.html:16
#, python-format
msgid ""
"\n"
"                <p class=\"next-date\"><strong>Nächster Einsendeschluss: "
"%(next_date)s</strong></p>\n"
"                "
msgstr ""

#: shorted/templates/pages/home.html:24
msgid "Aktuelle Gewinner"
msgstr "Current winners"

#: shorted/templates/pages/idea-form.html:8
msgid ""
"\n"
"                    Bitte füllen Sie die folgenden Formularfelder aus. Ein "
"bis zwei Wochen nach der nächsten Deadline\n"
"                    erhalten Sie von uns eine Mail, falls wir Ihren "
"Vorschlag auswählen. Bereits jetzt danken wir Ihnen herzlich\n"
"                    für Ihre Eingabe - auf die wir uns freuen!\n"
"                    "
msgstr ""
"\n"
"                    Please fill in the following form fields. One to two "
"weeks after the next deadline, you will receive an email                    "
"from us if we select your proposal. Thank you very much for your input - we "
"are looking forward to it!                    "

#: shorted/templates/pages/idea-form.html:22
msgid "Absenden"
msgstr "Submit"

#: shorted/templates/pages/impressum.html:7
msgid "Kontaktadresse"
msgstr "Contact address"

#: shorted/templates/pages/impressum.html:10
msgid ""
"\n"
"          New Media Center<br />\n"
"          Universität Basel<br />\n"
"          Totenztanz 18<br />\n"
"          4051 Basel<br />\n"
"          Schweiz<br /><br />\n"
"          Tel. +41 61 207 58 01<br />\n"
"          Email: contact-nmc@unibas.ch<br />\n"
"          "
msgstr ""
"\n"
"          New Media Center<br />\n"
"          Universität Basel<br />\n"
"          Totenztanz 18<br />\n"
"          4051 Basel<br />\n"
"          Schweiz<br /><br />\n"
"          Tel. +41 61 207 58 01<br />\n"
"          Email: contact-nmc@unibas.ch<br />\n"
"          "

#: shorted/templates/pages/impressum.html:25
msgid "Haftungsausschluss"
msgstr "Disclaimer"

#: shorted/templates/pages/impressum.html:28
msgid ""
"\n"
"          Der Autor übernimmt keinerlei Gewähr hinsichtlich der inhaltlichen "
"Richtigkeit, Genauigkeit, Aktualität,\n"
"          Zuverlässigkeit und Vollständigkeit der Informationen. "
"Haftungsansprüche gegen den Autor wegen Schäden\n"
"          materieller\n"
"          oder immaterieller Art, welche aus dem Zugriff oder der Nutzung "
"bzw. Nichtnutzung der veröffentlichten\n"
"          Informationen, durch Missbrauch der Verbindung oder durch "
"technische Störungen entstanden sind, werden\n"
"          ausgeschlossen. Alle Angebote sind unverbindlich. Der Autor behält "
"es sich ausdrücklich vor, Teile der\n"
"          Seiten oder\n"
"          das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu "
"ergänzen, zu löschen oder die\n"
"          Veröffentlichung\n"
"          zeitweise oder endgültig einzustellen.\n"
"          "
msgstr ""
"\n"
"          The author assumes no liability whatsoever with regard to the "
"correctness, accuracy, topicality,\n"
"          Reliability and completeness of the information. Liability claims "
"against the author for damages\n"
"          material\n"
"          or immaterial nature arising from access or use or non-use of the "
"published\n"
"          Information, by misuse of the connection or by technical "
"disturbances, are\n"
"          excluded. All offers are non-binding. The author expressly "
"reserves the right to make changes to parts of the\n"
"          Pages or\n"
"          the entire offer without separate announcement, to supplement, to "
"delete or\n"
"          Publication\n"
"          temporarily or permanently discontinued.\n"
"          "

#: shorted/templates/pages/impressum.html:46
msgid "Haftung für Links"
msgstr "Liability for links"

#: shorted/templates/pages/impressum.html:49
msgid ""
"\n"
"          Verweise und Links auf Webseiten Dritter liegen ausserhalb unseres "
"Verantwortungsbereichs Es wird jegliche\n"
"          Verantwortung für solche Webseiten abgelehnt. Der Zugriff und die "
"Nutzung solcher Webseiten erfolgen auf\n"
"          eigene\n"
"          Gefahr des Nutzers oder der Nutzerin.\n"
"          "
msgstr ""
"\n"
"          References and links to websites of third parties are outside our "
"area of responsibility. It is any\n"
"          Responsibility for such websites is rejected. Access and use of "
"such websites is at your own risk\n"
"          Risk of the user or the user.\n"
"          "

#: shorted/templates/pages/impressum.html:61
msgid "Urheberrechte"
msgstr "Copyright"

#: shorted/templates/pages/impressum.html:64
msgid ""
"\n"
"          Die Urheber- und alle anderen Rechte an Inhalten, Bildern, Fotos "
"oder anderen Dateien auf der Website\n"
"          gehören\n"
"          ausschliesslich der Universität Basel oder den speziell genannten "
"Rechtsinhabern. Für die Reproduktion\n"
"          jeglicher\n"
"          Elemente ist die schriftliche Zustimmung der Urheberrechtsträger "
"im Voraus einzuholen.\n"
"          "
msgstr ""
"\n"
"          The copyright and all other rights to content, images, photos or "
"other files on the website\n"
"          belong\n"
"          exclusively to the University of Basel or the specially named "
"rights holders. For the reproduction\n"
"          any\n"
"          Elements is the written consent of the copyright holders in "
"advance.\n"
"          "

#: shorted/templates/pages/impressum.html:77
msgid "Datenschutz"
msgstr "Privacy"

#: shorted/templates/pages/impressum.html:80
msgid ""
"\n"
"          Gestützt auf Artikel 13 der schweizerischen Bundesverfassung und "
"die datenschutzrechtlichen Bestimmungen des\n"
"          Bundes\n"
"          (Datenschutzgesetz, DSG) hat jede Person Anspruch auf Schutz ihrer "
"Privatsphäre sowie auf Schutz vor\n"
"          Missbrauch\n"
"          ihrer persönlichen Daten. Wir halten diese Bestimmungen ein. "
"Persönliche Daten werden streng vertraulich\n"
"          behandelt\n"
"          und weder an Dritte verkauft noch weiter gegeben. In enger "
"Zusammenarbeit mit unseren Hosting-Providern\n"
"          bemühen wir\n"
"          uns, die Datenbanken so gut wie möglich vor fremden Zugriffen, "
"Verlusten, Missbrauch oder vor Fälschung zu\n"
"          schützen.\n"
"          Beim Zugriff auf unsere Webseiten werden folgende Daten in "
"Logfiles gespeichert: IP-Adresse, Datum, Uhrzeit,\n"
"          Browser-Anfrage und allg. übertragene Informationen zum "
"Betriebssystem resp. Browser. Diese Nutzungsdaten\n"
"          bilden die\n"
"          Basis für statistische, anonyme Auswertungen, so dass Trends "
"erkennbar sind, anhand derer wir unsere\n"
"          Angebote\n"
"          entsprechend verbessern können.\n"
"          "
msgstr ""
"\n"
"          Based on Article 13 of the Swiss Federal Constitution and the data "
"protection provisions of the\n"
"          Federal\n"
"          (Data Protection Act, DSG), every person is entitled to protection "
"of their privacy and protection against\n"
"          abuse\n"
"          their personal data. We comply with these provisions. Personal "
"data will be treated strictly confidentially\n"
"          and neither sold to third parties nor passed on. In close "
"cooperation with our hosting providers\n"
"          we try\n"
"          to protect the databases as best as possible from external access, "
"loss, misuse or falsification.\n"
"          When accessing our websites, the following data is stored in log "
"files: IP address, date, time,\n"
"          Browser request and general information transmitted about the "
"operating system or browser. This usage data\n"
"          form the\n"
"          Basis for statistical, anonymous evaluations, so that trends are "
"recognizable, based on which we\n"
"          Offers\n"
"          can be improved accordingly.\n"
"          "

#: shorted/views.py:31
msgid "Ihre Idee wurde erfolgreich abgeschickt."
msgstr "Your idea has been successfully submitted."
